"""Stream type classes for tap-shoplazza."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_shoplazza.client import shoplazzaStream

# TODO: Delete this is if not using json files for schema definition
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
# TODO: - Override `UsersStream` and `GroupsStream` with your own stream definition.
#       - Copy-paste as many times as needed to create multiple stream types.


class ProductStream(shoplazzaStream):
    """Define custom stream."""
    name = "products"
    path = "/products?limit=50"
    primary_keys = ["id"]
    records_jsonpath = "$.products[*]"
    replication_key = None
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("title", th.StringType),
        th.Property("brief", th.StringType),
        th.Property("vendor", th.StringType),
        th.Property("vendor_url", th.StringType),
        th.Property("has_only_default_variant", th.BooleanType),
        th.Property("requires_shipping", th.BooleanType),
        th.Property("taxable", th.BooleanType),
        th.Property("inventory_tracking", th.BooleanType),
        th.Property("inventory_policy", th.StringType),
        th.Property("inventory_quantity", th.IntegerType),
        th.Property("published", th.BooleanType),
        th.Property("handle", th.StringType),
        th.Property("need_variant_image", th.BooleanType),
        th.Property("note", th.StringType),
        th.Property("description", th.StringType),
        th.Property("updated_at", th.StringType),
        th.Property("created_at", th.StringType),
        th.Property("spu", th.StringType),
        th.Property("fake_sales", th.IntegerType),
        th.Property("display_fake_sales", th.BooleanType),
        th.Property("tags", th.StringType),
        th.Property("image", th.ObjectType(
            th.Property("src", th.StringType),
            th.Property("width", th.IntegerType),
            th.Property("height", th.IntegerType),
            th.Property("path", th.StringType),
            th.Property("alt", th.StringType)

        )),
        th.Property("meta_title", th.StringType),
        th.Property("meta_description", th.StringType),
        th.Property("meta_keyword", th.StringType),
        th.Property("published_at", th.StringType),
        th.Property("variants", th.ArrayType(

            th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("product_id", th.StringType),
                th.Property("title", th.StringType),
                th.Property("sku", th.StringType),
                th.Property("position", th.NumberType),
                th.Property("option1", th.StringType),
                th.Property("option2", th.StringType),
                th.Property("option3", th.StringType),
                th.Property("barcode", th.StringType),
                th.Property("weight_unit", th.StringType),
                th.Property("inventory_quantity", th.IntegerType),
                th.Property("note", th.StringType),
                th.Property("created_at", th.StringType),
                th.Property("updated_at", th.StringType),
                th.Property("image_id", th.StringType),
                th.Property("image", th.ObjectType(

                th.Property("src", th.StringType),
                th.Property("width", th.IntegerType),
                th.Property("height", th.IntegerType),
                th.Property("path", th.StringType),
                th.Property("alt", th.StringType)

                )),
                th.Property("weight", th.StringType),
                th.Property("compare_at_price", th.StringType),
                th.Property("price", th.StringType)

            )


        )),
        th.Property("images", 
        th.ArrayType(
        th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("product_id", th.StringType),
                th.Property("position", th.IntegerType),

                th.Property("src", th.StringType),
                th.Property("width", th.IntegerType),
                th.Property("height", th.IntegerType),
                th.Property("alt", th.StringType),
                th.Property("created_at", th.StringType),
                th.Property("updated_at", th.StringType)

                )))
        
    ).to_dict()



class OrderStream(shoplazzaStream):
    """Define custom stream."""
    name = "orders"
    path = "/orders?limit=50"
    primary_keys = ["id"]
    records_jsonpath = "$.orders[*]"
    replication_key = None
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("note", th.StringType),
        th.Property("number", th.StringType),
        th.Property("financial_status", th.StringType),
        th.Property("status", th.StringType),
        th.Property("cancel_reason", th.StringType),
        th.Property("payment_method", th.StringType),
        th.Property("fulfillment_status", th.StringType),
        th.Property("discount_code", th.StringType),
        th.Property("discount_applications", th.StringType),
        th.Property("customer_note", th.StringType),
        th.Property("browser_ip", th.StringType),
        th.Property("landing_site", th.StringType),
        th.Property("buyer_accepts_marketing", th.BooleanType),
        th.Property("currency", th.StringType),
        th.Property("total_price", th.StringType),
        th.Property("total_discount", th.StringType),
        th.Property("total_tax", th.StringType),
        th.Property("total_shipping", th.StringType),
        th.Property("sub_total", th.StringType),
        th.Property("code_discount_total", th.StringType),
        th.Property("line_item_discount_total", th.StringType),
        th.Property("tags", th.StringType),
        th.Property("created_at", th.StringType),
        th.Property("updated_at", th.StringType),
        th.Property("canceled_at", th.StringType),
        th.Property("customer_deleted_at", th.StringType),
        th.Property("deleted_at", th.StringType),
        th.Property("placed_at", th.StringType),
        th.Property("total_refund_price", th.StringType),
        th.Property("total_refund_tax", th.StringType),
        th.Property("total_refund_discount", th.StringType),
        th.Property("refund_status", th.StringType),
        th.Property("shipping_line", th.ObjectType(

            th.Property("name", th.StringType)
        )),
        th.Property("customer", th.ObjectType(
            th.Property("email", th.StringType),
            th.Property("first_name", th.StringType),
            th.Property("last_name", th.StringType),
            th.Property("phone", th.StringType),
            th.Property("created_at", th.StringType),
            th.Property("updated_at", th.StringType),
            th.Property("orders_count", th.StringType),
            th.Property("total_spent", th.StringType)
       


        )),
        th.Property("shipping_address", th.ObjectType(
            th.Property("first_name", th.StringType),
            th.Property("last_name", th.StringType),
            th.Property("phone", th.StringType),
            th.Property("city", th.StringType),
            th.Property("zip", th.StringType),
            th.Property("province", th.StringType),
            th.Property("country", th.StringType),
            th.Property("company", th.StringType),
            th.Property("latitude", th.StringType),
            th.Property("longitude", th.StringType),
            th.Property("country_code", th.StringType),
            th.Property("province_code", th.StringType),
            th.Property("email", th.StringType),
            th.Property("phone_area_code", th.StringType),
            th.Property("area", th.StringType),
            th.Property("address1", th.StringType),
            th.Property("address2", th.StringType),
            th.Property("name", th.StringType),
            th.Property("extra_info", th.StringType)


        )),
        th.Property("billing_address", th.StringType),
        th.Property("payment_line", th.StringType),
        th.Property("line_items", 
        th.ArrayType(
        
        th.ObjectType(
            th.Property("id", th.StringType),
            th.Property("product_id", th.StringType),
            th.Property("variant_id", th.StringType),
            th.Property("variant_title", th.StringType),
            th.Property("product_title", th.StringType),
            th.Property("product_handle", th.StringType),
            th.Property("quantity", th.IntegerType),
            th.Property("note", th.StringType),
            th.Property("fulfillment_status", th.StringType),
            th.Property("sku", th.StringType),
            th.Property("weight_unit", th.StringType),
            th.Property("vendor", th.StringType),
            th.Property("product_url", th.StringType),
            th.Property("compare_at_price", th.StringType),
            th.Property("image", th.StringType),
            th.Property("price", th.StringType),
            th.Property("total", th.StringType),
            th.Property("weight", th.StringType),
            th.Property("properties", th.ArrayType(

                th.ObjectType(
                    th.Property("name", th.StringType),
                    th.Property("value", th.StringType),


                )
            )),
            th.Property("custom_properties", th.StringType),
            th.Property("refund_quantity", th.IntegerType),
            th.Property("refund_total", th.StringType),
            

        )))    
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "order_id": record["id"],
        }




class ShopDetailsStream(shoplazzaStream):
    """Define custom stream."""
    name = "shop"
    path = "/shop"
    primary_keys = ["id"]
    records_jsonpath = "$.shop[*]"
    replication_key = None

    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("email", th.StringType),
        th.Property("country_code", th.StringType),
        th.Property("province_code", th.StringType),
        th.Property("address1", th.StringType),
        th.Property("address2", th.StringType),
        th.Property("zip", th.StringType),
        th.Property("city", th.StringType),
        th.Property("phone", th.StringType),
        th.Property("primary_locale", th.StringType),
        th.Property("currency", th.StringType),
        th.Property("customer_email", th.StringType),
        th.Property("timezone", th.StringType),
        th.Property("domain", th.StringType),
        th.Property("shop_owner", th.StringType),
        th.Property("account", th.StringType),
        th.Property("icon", th.ObjectType(

            th.Property("src", th.StringType),
            th.Property("alt", th.StringType),
            th.Property("path", th.StringType),
        )),
        th.Property("created_at", th.StringType),
        th.Property("updated_at", th.StringType),
       

    ).to_dict()


class FulfillmentsStream(shoplazzaStream):
    """Define custom stream."""
    name = "fulfillments"
    path = "/orders/{order_id}/fulfillments?limit=50"
    primary_keys = ["id"]
    records_jsonpath = "$.fulfillments[*]"
    replication_key = None
    parent_stream_type = OrderStream
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("order_id", th.StringType),
        th.Property("status", th.StringType),
        th.Property("created_at", th.StringType),
        th.Property("updated_at", th.StringType),
        th.Property("tracking_company", th.StringType),
        th.Property("tracking_number", th.StringType),
        th.Property("tracking_company_code", th.StringType),
        th.Property("line_items", th.ArrayType(

            th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("product_id", th.StringType),
                th.Property("product_title", th.StringType),
                th.Property("variant_id", th.StringType),
                th.Property("variant_title", th.StringType),
                th.Property("quantity", th.IntegerType),
                th.Property("note", th.StringType),
                th.Property("image", th.StringType),
                th.Property("price", th.NumberType),
                th.Property("compare_at_price", th.NumberType),
                th.Property("total", th.StringType),
                th.Property("fulfillment_status", th.StringType),
                th.Property("sku", th.StringType),
                th.Property("weight", th.NumberType),
                th.Property("weight_unit", th.StringType),
                th.Property("vendor", th.StringType),
                th.Property("product_handle", th.StringType),
                th.Property("product_url", th.StringType)
            )


        )),
        th.Property("properties", th.ArrayType(
            th.ObjectType(

            th.Property("name", th.StringType),
            th.Property("value", th.StringType)
        )))
       

    ).to_dict()